<?php

namespace AppBundle\Form;

namespace AppBundle\Form;
use AppBundle\Entity\Poblacion;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PoblacionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                "nombre",
                TextType::class,[
                "attr" =>[
                    "class" => "form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus rounded-0 g-px-14 g-py-10"
                ],"required" => true])
            ->add(
                "provincia",
                ChoiceType::class,
                ["choices" =>[
                    "Tarragona" => "Tarragona",
                    "Barcelona" => "Barcelona",
                    "Lerida"    => "Lerida",
                    "Gerona"    => "Gerona"
                ],
                "attr" =>[
                    "class" => "js-select u-select--v3-select u-sibling w-100"
                ]])
            ->add("descripcion")
            ->add("imagen")
            ->add("escudo")
            ->add(
                "pos_lat",
                NumberType::class,[
                "attr" =>[
                    "class" => ""
                ]])
            ->add(
                "pos_lng",
                NumberType::class,[
                "attr" =>[
                    "class" => ""
                ]])
            ->add(
                "visible",
                CheckboxType::class,[
                "attr" =>[
                    "class" => "g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                ],"required" => false
                ])
            ->add(
                "guardar",
                SubmitType::class,[
                "attr" =>[
                    "class" => "btn btn-primary"
                ]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Poblacion'
        ]);
    }
}