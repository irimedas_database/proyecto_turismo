<?php
/**
 * Created by PhpStorm.
 * User: irime
 * Date: 15/05/2019
 * Time: 10:54
 */

namespace AppBundle\Form;


use AppBundle\Entity\LocalizacionTipo;
use AppBundle\Entity\Poblacion;
use AppBundle\Repository\PoblacionRepository;
use Doctrine\ORM\EntityRepository;
use AppBundle\Repository\LocalizacionTipoRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("usuario")
            ->add("password",
                PasswordType::class)
            ->add("acceder",
                SubmitType::class, [
                    "attr" =>[
                        "class" => "btn btn-primary"
                    ]
                ]);
    }


}