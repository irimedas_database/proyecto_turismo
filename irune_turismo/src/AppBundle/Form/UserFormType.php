<?php

namespace AppBundle\Form;


use AppBundle\Entity\Poblacion;
use AppBundle\Repository\PoblacionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("username",
                TextType::class, [
                    "attr" =>[
                        "class" => "form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus rounded-0 g-px-14 g-py-10"
                    ],"required" => true
                ])
            ->add("password",
                TextType::class, [
                    "attr" =>[
                        "class" => "form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus rounded-0 g-px-14 g-py-10"
                    ],"required" => true
                ])
            ->add("email",
                TextType::class, [
                    "attr" =>[
                        "class" => "form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus rounded-0 g-px-14 g-py-10"
                    ],"required" => false
                ])
            ->add("poblacion",
                EntityType::class, [
                    'placeholder' => 'Elije una población',
                    "class" => Poblacion::class,
                    "query_builder" => function(PoblacionRepository $repo){
                        return $repo->ordenarPorIdQueryBuilder();
                    },
                    "attr" =>[
                        "class" => "js-select u-select--v3-select u-sibling w-100"
                    ],"required" => false
                ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'Administrador' => 'ROLE_ADMIN',
                    'Manager' => 'ROLE_MANAGER',
                ],
            ])
            ->add("guardar",
                SubmitType::class, [
                    "attr" =>[
                        "class" => "btn btn-primary"
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User'
        ]);
    }


}