<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PoblacionRepository")
 * @ORM\Table(name="poblacion")
 */

class Poblacion
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string")
     */
    private $provincia;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $imagen;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $escudo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pos_lat = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pos_lng = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = false;

    //-------------------------------------

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getEscudo()
    {
        return $this->escudo;
    }

    /**
     * @param mixed $escudo
     */
    public function setEscudo($escudo)
    {
        $this->escudo = $escudo;
    }

    /**
     * @return mixed
     */
    public function getPosLat()
    {
        return $this->pos_lat;
    }

    /**
     * @param mixed $pos_lat
     */
    public function setPosLat($pos_lat)
    {
        $this->pos_lat = $pos_lat;
    }

    /**
     * @return mixed
     */
    public function getPosLng()
    {
        return $this->pos_lng;
    }

    /**
     * @param mixed $pos_lng
     */
    public function setPosLng($pos_lng)
    {
        $this->pos_lng = $pos_lng;
    }

    /**
     * @return mixed
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }
}