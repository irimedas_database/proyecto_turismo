<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="localizacion")
 */
class Localizacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="Poblacion")
     */
    private $poblacion;

    /**
     * @ORM\ManyToOne(targetEntity="LocalizacionTipo")
     */
    private $tipo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $imagen;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $contacto;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $horario;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pos_lat = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pos_lng = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = false;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getPoblacion()
    {
        return $this->poblacion;
    }

    /**
     * @param mixed $poblacion
     */
    public function setPoblacion(Poblacion $poblacion)
    {
        $this->poblacion = $poblacion;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo(LocalizacionTipo $tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * @param mixed $contacto
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;
    }

    /**
     * @return mixed
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * @param mixed $horario
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;
    }

    /**
     * @return mixed
     */
    public function getPosLat()
    {
        return $this->pos_lat;
    }

    /**
     * @param mixed $pos_lat
     */
    public function setPosLat($pos_lat)
    {
        $this->pos_lat = $pos_lat;
    }

    /**
     * @return mixed
     */
    public function getPosLng()
    {
        return $this->pos_lng;
    }

    /**
     * @param mixed $pos_lng
     */
    public function setPosLng($pos_lng)
    {
        $this->pos_lng = $pos_lng;
    }

    /**
     * @return mixed
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

}