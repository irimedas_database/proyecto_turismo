<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LocalizacionTipo;
use AppBundle\Entity\Poblacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{
    public function obtenerPosicionPoblacionAction(Request $request){
       $id = $request->get("id");

       $em = $this->getDoctrine()->getManager();
       $poblacion_db = $em->getRepository(Poblacion::class)->find($id);

       $response = array(
           "pos_lat" => $poblacion_db->getPosLat(),
           "pos_lng" => $poblacion_db->getPosLng());

       echo json_encode($response);
       exit;
    }

    public function obtenerImagenMarcadorAction(Request $request){
        $id = $request->get("id");

        $em = $this->getDoctrine()->getManager();
        $tipo = $em->getRepository(LocalizacionTipo::class)->find($id);

        $response = array(
            "imagen_marcador" => $tipo->getMarcador());

        echo json_encode($response);
        exit;
    }
}