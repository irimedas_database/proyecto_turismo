<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Localizacion;
use AppBundle\Entity\LocalizacionTipo;
use AppBundle\Entity\Poblacion;
use AppBundle\Entity\User;
use AppBundle\Form\LocalizacionFormType;
use AppBundle\Form\PoblacionFormType;
use AppBundle\Form\UserFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BackendController extends Controller
{
    /* Apartados relacionados con la Población */
    public function listarPoblacionAction(Request $request){

//        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $pobliaciones_db = $em->getRepository(Poblacion::class)->findAll();
        dump($pobliaciones_db);

        return $this->render("AppBundle:Backend:poblacion/list.html.twig",
            array(
                "poblaciones" => $pobliaciones_db
            ));
    }

    public function crearPoblacionAction(Request $request)
    {
        $form = $this->createForm(PoblacionFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {

            $poblacion = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($poblacion);
            $em->flush();
            $this->addFlash('success', 'Población creada con exito!');
            return $this->redirectToRoute("mostrar_poblaciones");
        }

        return $this->render("AppBundle:Backend:poblacion/editar.html.twig",
            array(
                "poblacionForm" => $form->createView(),
                "tipo" => 0
            ));
    }

    public function editarPoblacionAction(Request $request, Poblacion $poblacion)
    {
        $form = $this->createForm(PoblacionFormType::class, $poblacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $poblacion_db = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($poblacion_db);
            $em->flush();

            $pob_name = $poblacion_db->getNombre();

            $this->addFlash("success", "La población '" . $pob_name . "' ha sido modificada con exito!");
            return $this->redirectToRoute("mostrar_poblaciones");
        }

        return $this->render("AppBundle:Backend:poblacion/editar.html.twig",
            array(
                "poblacionForm" => $form->createView(),
                "tipo" => 1
            ));
    }

    public function eliminarPoblacionAction(Request $request, Poblacion $poblacion){

        $em = $this->getDoctrine()->getManager();
        $poblacion_db = $em->getRepository(Poblacion::class)->find($poblacion);

        if (!$poblacion_db) {
            throw $this->createNotFoundException(
                'No hay ninguna población asociada a la siguiente id: ' . $poblacion->getId()
            );
        }

        $pob_name = $poblacion->getNombre();

        $em->remove($poblacion_db);
        $em->flush();
        $this->addFlash("success", "La población '" . $pob_name . "' ha sido eliminada con exito!");
        return $this->redirectToRoute('mostrar_poblaciones');
    }

    public function visualizarPoblacionAction(Request $request, Poblacion $poblacion){

        $em = $this->getDoctrine()->getManager();
        $poblacion_db = $em->getRepository(Poblacion::class)->find($poblacion);

        $visible = $poblacion_db->getVisible();

        if ($visible == false || $visible == null){
            $poblacion_db->setVisible(true);
            $accion = "activada";
        }else{
            $poblacion_db->setVisible(false);
            $accion = "desactivada";
        }

        $em->persist($poblacion_db);
        $em->flush();

        $pob_name = $poblacion_db->getNombre();

        $this->addFlash("success", "La población '" . $pob_name . "' ha sido ".$accion." con exito!");
        return $this->redirectToRoute('mostrar_poblaciones');

    }

    /* Apartados relacionados con las Localizaciones */
    public function listarLocalizacionAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $localizaciones_db = $em->getRepository(Localizacion::class)->findAll();
        dump($localizaciones_db);

        return $this->render("AppBundle:Backend:localizacion/list.html.twig",
            array(
                "localizaciones" => $localizaciones_db
            ));
    }

    public function crearLocalizacionAction(Request $request){

        $form = $this->createForm(LocalizacionFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $localizacion = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($localizacion);
            $em->flush();
            $this->addFlash('success', 'Localización creada con exito!');
            return $this->redirectToRoute("mostrar_localizaciones");
        }

        return $this->render("AppBundle:Backend:localizacion/editar.html.twig",
            array(
                "localizacionForm" => $form->createView(),
                "tipo" => 0
            ));
    }

    public function editarLocalizacionAction(Request $request, Localizacion $localizacion){

        $form = $this->createForm(LocalizacionFormType::class, $localizacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $localizacion_db = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($localizacion_db);
            $em->flush();

            $loc_name = $localizacion_db->getNombre();

            $this->addFlash("success", "La Localización '" . $loc_name . "' ha sido modificada con exito!");
            return $this->redirectToRoute("mostrar_localizaciones");
        }

        return $this->render("AppBundle:Backend:localizacion/editar.html.twig",
            array(
                "localizacionForm" => $form->createView(),
                "tipo" => 1
            ));

    }

    public function eliminarLocalizacionAction(Request $request, Localizacion $localizacion){

        $em = $this->getDoctrine()->getManager();
        $localizacion_db = $em->getRepository(Localizacion::class)->find($localizacion);

        if (!$localizacion_db) {
            throw $this->createNotFoundException(
                'No hay ninguna locallización asociada a la siguiente id: ' . $localizacion->getId()
            );
        }

        $loc_name = $localizacion->getNombre();

        $em->remove($localizacion_db);
        $em->flush();

        $this->addFlash("success", "La localización '" . $loc_name . "' ha sido eliminada con exito!");
        return $this->redirectToRoute('mostrar_localizaciones');
    }

    public function visualizarLocalizacionAction(Request $request, Localizacion $localizacion){
        $em = $this->getDoctrine()->getManager();
        $localizacion_db = $em->getRepository(Localizacion::class)->find($localizacion);

        $visible = $localizacion_db->getVisible();

        if ($visible == false || $visible == null){
            $localizacion_db->setVisible(true);
            $accion = "activada";
        }else{
            $localizacion_db->setVisible(false);
            $accion = "desactivada";
        }

        $em->persist($localizacion_db);
        $em->flush();

        $loc_name = $localizacion_db->getNombre();

        $this->addFlash("success", "La localización '" . $loc_name . "' ha sido ".$accion." con exito!");
        return $this->redirectToRoute('mostrar_localizaciones');
    }

    /* Apartados relacionados con los Usuarios */
    public function listarUsuariosAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $usuarios_db = $em->getRepository(User::class)->findAll();
        dump($usuarios_db);

        return $this->render("AppBundle:Backend:usuarios/list.html.twig",
            array(
                "usuarios" => $usuarios_db
            ));
    }

    public function crearUsuarioAction(Request $request){

        $form = $this->createForm(UserFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Usuario creado con exito!');
            return $this->redirectToRoute("mostrar_usuarios");
        }

        return $this->render("AppBundle:Backend:usuarios/editar.html.twig",
            array(
                "userForm" => $form->createView(),
                "tipo" => 0
            ));
    }

    public function editarUsuarioAction(Request $request, User $user){

        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user_db = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user_db);
            $em->flush();

            $user_name = $user_db->getUsername();

            $this->addFlash("success", "El usuario '" . $user_name . "' ha sido modificado con exito!");
            return $this->redirectToRoute("mostrar_usuarios");
        }

        return $this->render("AppBundle:Backend:usuarios/editar.html.twig",
            array(
                "userForm" => $form->createView(),
                "tipo" => 1
            ));

    }

    public function eliminarUsuarioAction(Request $request, User $user){

        $em = $this->getDoctrine()->getManager();
        $user_db = $em->getRepository(User::class)->find($user);

        if (!$user_db) {
            throw $this->createNotFoundException(
                'No hay ningún usuario asociado a la siguiente id: ' . $user->getId()
            );
        }

        $user_name = $user->getUsername();

        $em->remove($user_db);
        $em->flush();

        $this->addFlash("success", "El usuario '" . $user_name . "' ha sido eliminado con exito!");
        return $this->redirectToRoute('mostrar_usuarios');
    }

    /* Apartados relacionados con el Mapa */
    public function editarMapaAction(Request $request){

        return $this->render("AppBundle:Backend:mapa/editar.html.twig", array());
    }

    /* Otros apartados */
    public function obtenerPosicionamiento(Request $request){
        $em = $this->getDoctrine()->getManager();

        dump($request);
        exit;
    }

}