<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Localizacion;
use AppBundle\Entity\LocalizacionTipo;
use AppBundle\Entity\Poblacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class FrontendController extends Controller
{
    public function homeAction(Request $request)
    {

        $em = $em = $this->getDoctrine()->getManager();
        $pobliaciones_db = $em->getRepository(Poblacion::class)->findBy(array("visible" => true));

        return $this->render("AppBundle:Frontend:home.html.twig", array(
            "poblaciones" => $pobliaciones_db
        ));
    }

    public function mostrarInfoPoblacionAction(Request $request){

        $poblacion_name = $request->get("poblacion_name");

        $em = $em = $this->getDoctrine()->getManager();

        $pobliacion = $em->getRepository(Poblacion::class)->findOneBy(array("nombre" => $poblacion_name));
        $localizaciones =  $em->getRepository(Localizacion::class)->findBy(array(
            "poblacion" => $pobliacion->getId(),
            "visible" => true));

        $tipos = $em->getRepository(LocalizacionTipo::class)->findAll();

        $localizaciones_tipo = array();
        $num = 1;

        foreach($tipos as $tipo){

            $localizacion_tipo =  $em->getRepository(Localizacion::class)->findBy(array(
                "poblacion" => $pobliacion->getId(),
                "visible" => true,
                "tipo" => $tipo->getId()));

            $localizaciones_tipo[$num] = $localizacion_tipo;
            $num += 1;
        }

        return $this->render("AppBundle:Frontend:localizacion.html.twig",
            array(
                "poblacion"      => $pobliacion,
                "localizaciones" => $localizaciones,
                "localizaciones_tipo" => $localizaciones_tipo
            )
        );
    }
}