<?php

namespace AppBundle\Repository;

use AppBundle\Entity\LocalizacionTipo;
use Doctrine\ORM\EntityRepository;

class LocalizacionTipoRepository extends EntityRepository
{
    public function ordenarPorIdQueryBuilder()
    {
        return $this->createQueryBuilder('localizacion_tipo')
            ->orderBy('localizacion_tipo.id', 'ASC');
    }
}