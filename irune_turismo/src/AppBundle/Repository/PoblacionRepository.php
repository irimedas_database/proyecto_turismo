<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Poblacion;
use Doctrine\ORM\EntityRepository;

class PoblacionRepository extends EntityRepository
{
    public function ordenarPorIdQueryBuilder()
    {
        return $this->createQueryBuilder('poblacion')
            ->orderBy('poblacion.id', 'ASC');
    }
}